import gi
import subprocess
import datetime
gi.require_version('Gtk', '3.0')
from gi.repository import Gtk,  Gdk

class LauraTaskbar(Gtk.Window):

    def __init__(self):
        # get_res
        res_output = subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4',shell=True, stdout=subprocess.PIPE).communicate()[0]
        resolution = res_output.split()[0].split(b'x')
        res_width = int(resolution[0])
        res_height = int(resolution[1])
        # gtk_window
        Gtk.Window.__init__(self, title="Laura Taskbar")
        Gtk.Window.set_resizable(self, False)
        Gtk.Window.set_decorated(self, False)
        Gtk.Window.move(self, 0,  res_height)
        Gtk.Window.set_size_request(self, res_width, 32)
        Gtk.Window.set_keep_above(self, True)
        Gtk.Window.set_border_width(self,  0)
        # gtk_window_style
        Gtk.Window.override_background_color(self, Gtk.StateFlags.NORMAL, Gdk.RGBA(3.0, 3, 2, 0.29))
        Gtk.Window.modify_fg(self, Gtk.StateFlags.NORMAL, Gdk.color_parse("white"))
        # gtk_coms
        self.l_time = Gtk.Label(datetime.datetime.now().time())
        self.add(self.l_time)
        self.add(self.start)


class LauraStart(Gtk.Window):

    def __init__(self):
        # get_res
        res_output = subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4',shell=True, stdout=subprocess.PIPE).communicate()[0]
        resolution = res_output.split()[0].split(b'x')
        res_height = int(resolution[1])
        # gtk_window
        Gtk.Window.__init__(self, title="Laura Top")
        Gtk.Window.set_resizable(self, False)
        Gtk.Window.set_decorated(self, False)
        Gtk.Window.move(self, 0,  48)
        Gtk.Window.set_size_request(self, 64, (res_height - 48))
        Gtk.Window.set_keep_above(self, True)
        Gtk.Window.set_border_width(self,  0)
        # gtk_window_style
        Gtk.Window.override_background_color(self, Gtk.StateFlags.NORMAL, Gdk.RGBA(3.0, 3, 2, 0.29))
        Gtk.Window.modify_fg(self, Gtk.StateFlags.NORMAL, Gdk.color_parse("white"))


# Taskbar
aTop = LauraTaskbar()
aTop.connect("delete-event", Gtk.main_quit)
aTop.show_all()

'''# Start
aLeft = LauraStart()
aLeft.connect("delete-event", Gtk.main_quit)
aLeft.show_all()'''

Gtk.main()
