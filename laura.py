import gi
import subprocess
import datetime
import glob
import os
import cairo
import numpy as np
from pprint import pprint
import re
from wmctrl import Window

gi.require_version('Gtk', '3.0')
from gi.repository import Gtk, Gdk

# Theme load
style_provider = Gtk.CssProvider()
theme = open('common/laura.css', 'rb')
css_data = theme.read()
theme.close()
style_provider.load_from_data(css_data)
Gtk.StyleContext.add_provider_for_screen(
    Gdk.Screen.get_default(), style_provider,
    Gtk.STYLE_PROVIDER_PRIORITY_APPLICATION
)


# Common
class Common:
    @staticmethod
    def get_applications():
        print("le apps sono in dir: etc/xdg/menus/gnome-applications.menu")

    @staticmethod
    def get_res():
        res_output = \
            subprocess.Popen('xrandr | grep "\*" | cut -d" " -f4', shell=True, stdout=subprocess.PIPE).communicate()[0]
        resolution = res_output.split()[0].split(b'x')
        return resolution

    @staticmethod
    def set_datetime():
        Taskbar.label_datetime.set_text(common.get_datetime())

    @staticmethod
    def get_datetime():
        datatime = datetime.datetime.now().strftime('%H:%M')
        datadate = datetime.date.today().strftime('%a %d %b')
        return '%s %s' % (datadate.title(), str(datatime))

    @staticmethod
    def find_between(self, s, first, last):
        try:
            start = s.index(first) + len(first)
            end = s.index(last, start)
            return s[start:end]
        except ValueError:
            return ""


common = Common()
common.get_applications()


# Windows
class Windows:
    winctrl = Window

    def list(self):
        # pprint(self.winctrl.list())
        return self.winctrl.list()

    def add_windows(self):
        for w in self.list():
            wm_name = (w.wm_name[:16] + '..') if len(w.wm_name) > 16 else w.wm_name
            wm_class = re.match(r'^.*?\.', w.wm_class).group(0)[:-1].lower()
            # print(wm_class)
            window = Gtk.Button.new_from_icon_name(wm_class, Gtk.IconSize.MENU)
            window.set_label(" " + wm_name)
            window.set_alignment(0, 0)
            window.set_property("always_show_image", True)
            if Window.get_active().id == w.id:
                window.get_style_context().add_class("Window_selected")
            window.set_size_request(180, 32)
            window.set_property("name", w.id)
            window.connect("clicked", self.on_wm_clicked)
            Taskbar.windows_list.add(window)

    @staticmethod
    def on_wm_clicked(widget):
        window_id = widget.get_property("name")
        for child in Taskbar.windows_list.get_children():
            child.get_style_context().remove_class("Window_selected")
        if Window.get_active().id != window_id:
            widget.get_style_context().add_class("Window_selected")
            subprocess.call(["xdotool", "windowactivate", window_id])
        else:
            window_state = subprocess.Popen('xwininfo -all -id ' + window_id + ' | grep "Hidden"', shell=True,
                                            stdout=subprocess.PIPE).communicate()[0]
            window_state = window_state.replace(" ", "")
            print(window_state)
            if "Hidden" in window_state:
                widget.get_style_context().add_class("Window_selected")
                subprocess.call(["xdotool", "windowactivate", window_id])
            else:
                widget.get_style_context().remove_class("Window_selected")
                subprocess.call(["xdotool", "windowminimize", window_id])


windows = Windows()


# Start
class Start:
    start_visible = False
    taskbar_builder = Gtk.Builder()
    taskbar_builder.add_from_file("ui/taskbar.glade")

    # instances
    start_window = taskbar_builder.get_object("start")
    start_sandbox = taskbar_builder.get_object("start_sandbox")
    start_panel = taskbar_builder.get_object("start_grid")
    start_applications = taskbar_builder.get_object("start_applications")
    start_search = taskbar_builder.get_object("start_search")

    # desktop params
    screen = start_window.get_screen()
    visual = screen.get_rgba_visual()

    if visual and screen.is_composited():
        start_window.set_visual(visual)
        start_window.transparency = True
    else:
        print('System doesn\'t support compositor')
        start_window.set_visual(screen.get_system_visual)

    def init(self, *args):
        # names
        Start.start_window.set_name("Start")
        Start.start_panel.set_name("Start_panel")
        Start.start_sandbox.set_name("Start_sandbox")
        Start.start_applications.set_name("Start_applications")
        Start.start_search.set_name("Start_search")

        # decorate panel
        Start.start_window.transparency = False
        Start.start_window.set_app_paintable(True)
        Start.start_window.set_decorated(False)
        Start.start_window.move(0, int(common.get_res()[1]) - 32 - 600)
        Start.start_window.set_size_request(400, 600)
        Start.start_window.set_keep_above(True)
        Start.start_window.set_border_width(0)

        # build
        Start.start_window.show_all()
        Start.start_window.hide()
        start_visible = False


start = Start()
start.init()


# Taskbar
class Taskbar:
    taskbar_builder = Gtk.Builder()
    taskbar_builder.add_from_file("ui/taskbar.glade")

    # instances
    taskbar_window = taskbar_builder.get_object("taskbar")
    taskbar_sandbox = taskbar_builder.get_object("sandbox")
    windows_list = taskbar_builder.get_object("windows_list")
    button_start = taskbar_builder.get_object("btn_start")
    label_datetime = taskbar_builder.get_object("label_datetime")
    taskbar_systemtray = taskbar_builder.get_object("systemtray")
    column_datetime = taskbar_builder.get_object("column_datetime")
    toggle_click = taskbar_builder.get_object("toggle_click")

    # desktop params
    screen = taskbar_window.get_screen()
    visual = screen.get_rgba_visual()

    @staticmethod
    def on_start_clicked(widget):
        if start.start_visible:
            start.start_window.hide()
            widget.get_style_context().remove_class("Start_open")
            start.start_visible = False
        else:
            start.start_window.show()
            widget.get_style_context().add_class("Start_open")
            start.start_visible = True

    if visual and screen.is_composited():
        taskbar_window.set_visual(visual)
        taskbar_window.transparency = True
    else:
        print('System doesn\'t support compositor')
        taskbar_window.set_visual(screen.get_system_visual)

    def init(self, *args):
        # names
        Taskbar.button_start.set_name("Start_btn")
        Taskbar.taskbar_window.set_name("Taskbar")
        Taskbar.taskbar_sandbox.set_name("Sandbox")
        Taskbar.windows_list.set_name("Windows_list")
        Taskbar.taskbar_systemtray.set_name("System_tray")
        Taskbar.label_datetime.set_name("Date_time")

        # add windows to taskbar
        windows.add_windows()

        # decorate panel
        Taskbar.taskbar_window.transparency = False
        Taskbar.taskbar_window.set_app_paintable(True)
        Taskbar.taskbar_window.set_decorated(False)
        Taskbar.taskbar_window.move(0, int(common.get_res()[1]))
        Taskbar.taskbar_window.set_size_request(int(common.get_res()[0]), 32)
        Taskbar.taskbar_sandbox.set_size_request(int(common.get_res()[0]), 32)
        Taskbar.taskbar_window.set_keep_above(True)
        Taskbar.taskbar_window.set_border_width(0)

        # populate datetime
        common.set_datetime()

        # connectors
        Taskbar.button_start.connect("clicked", self.on_start_clicked)

        # build
        Taskbar.taskbar_window.show_all()


t = Taskbar()
t.init()
print "http://stackoverflow.com/questions/36279301/real-time-clock-display-in-tkinter"
Gtk.main()
